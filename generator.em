\version 0.1

# Expect:
# 1.
# 1.3.5.7.9.
# 4.16.36.64.
# 2.6.12.20.30.

ones = [
    next ^ = { 1 }
]

increasing ^start increment = [
    acc = start
    next ^ = {
        this.acc = this.acc + increment
        this.acc - increment
    }
]

doubler ^g = [
    next ^ = { 2 * g.next() }
]

squarer ^g = [
    next ^ = { v = g.next(); v * v }
]

summer ^g h = [
    next ^ = { g.next() + h.next() }
]

println: ones.next()

fours = squarer: doubler: increasing 1 1
xsqplusx = summer (squarer: increasing 1 1) (increasing 1 1)

inc = increasing 1 2
print: inc.next()
print: do: inc.next
print: inc.next()
print: inc.next()
println: inc.next()

print: fours.next()
print: fours.next()
print: fours.next()
println: fours.next()

print: xsqplusx.next()
print: xsqplusx.next()
print: xsqplusx.next()
print: xsqplusx.next()
println: xsqplusx.next()
