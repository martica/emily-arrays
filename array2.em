# Expect:
# 1.
# 4.

array = [
    # Can't use eq on atoms
    starter = [
        magicstart = true
    ]
    ender = [
        magicend = true
    ]
    empty = [
    ]
    isStart ^elem = {
        this.matchAtom elem `this.starter
    }
    isEnd ^elem = {
        this.matchAtom elem `this.ender
    }
    matchAtom ^elem object = {
        object.has elem && not (this.empty.has elem)
    }
    appendX ^elem = {
        this.isStart  elem ? this.appendY : (this.append(elem); this)
    }
    appendY ^elem = {
        this.isEnd elem ? this : (this.append(elem) ; this.appendY)
    }
]

println: (array.appendX 0).count
println: (array.appendX .magicstart 1 2 3 .magicend).count
