\version 0.1
# Make some arrays tools
#
# Expect:
# 0.
# 21.
# -21.
# [1., 2., 3., 4., 5., 6.]
# [1., 2., 3., 4., 5., 6., 1., 2., 3., 4., 5., 6.]
# [1., 2., 3., 4., 5., 6., 1., 2., 3., 4., 5., 6.]
# 6.
# [1., 2., 3., 4., 5., 6.]
# 1.
# [2., 3., 4., 5., 6.]
# [a, b, c, d, e, f]
# [a, b, c, d, e, f, 1., 2., 3., 4., 5., 6.]
# [a, b, c, d, e, f, g, 1., 2., 3., 4., 5., 6.]
# [<null>]
# [<null>]

inherit ^class = [ parent = class ]

array = [ 
    appendMulti ^elem = {
       this.append(elem)
       this.appendMulti
    }
    appendArray ^that = {
        that.each: this.append
    }
    append ^x = {
        super.append(x)
        this
    }
    plus ^that = {
        new = this.duplicate()
        new.appendArray that
        new
    }
    first ^ = {
       if (this.has 0) ^(this 0)
    }
    rest ^ = {
       new = inherit array
       first = true
       this.each ^x (if !first ^(new.append(x)); first = null)
       new
    }
    print ^ = {
       print "["
       print: this.first()
       this.rest().each ^x (print ", " x)
       print "]"
    }
    println ^ = {
       this.print()
       print ln
    }
    duplicate ^ = {
        new = inherit array
        this.each: new.append
        new
    }
]

foldl ^op init list = {
    (list.has 0) ? (foldl op (init op `list 0) (list.rest())) : init
}

numbers = inherit array
numbers.appendMulti 1 2 3 4 5 6

println (foldl .plus 0 array)
println (foldl .plus 0 numbers)
println (foldl .minus 0 numbers)
morenumbers = (foldl .append (inherit array) numbers.duplicate())
morenumbers.println()
evenmore = (foldl .append morenumbers numbers.duplicate())
evenmore.println()
morenumbers.println()

println (numbers.count)
numbers.println()
println (numbers.first())
numbers.rest().println()

letters = inherit array
letters.appendMulti "a" "b" "c" "d" "e" "f"
letters2 = letters.duplicate()

letters2.append("g")
letters.println()

letters.appendArray numbers
letters.println()

(letters2 + numbers).println()

empty = inherit array
empty.println()
empty.rest().println()
