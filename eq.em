\version 0.1
# This only makes sense on emily 0.1. Any changes to the language will
# likely break it as it depends on all kinds of crazy assumptions
# about which atoms are present on which types
#
# Expect:
# genericEq .hello .hello: <true>
# genericEq .hello .goodbye: <null>
# genericEq .goodbye .hello: <null>
# genericEq .parent .hello: <null>
# genericEq .hello .parent: <null>
# genericEq .parent .parent: <true>
# genericEq .has .let: <null>
# genericEq .set .set: <true>
# genericEq '1' '1': <true>
# genericEq 1 '1': <null>
# genericEq 1 1: <true>
# genericEq '1' 1: <null>
# genericEq 'a' .a: <null>
# genericEq null null: <true>
# genericEq true true: <true>
# genericEq true null: <null>
# genericEq null true: <null>
# genericEq genericEq true: <null>
# genericEq genericEq genericEq: <true>
# genericEq genericEq idAtom: <null>
# 
# atom ids
# --------
# has    1.
# and    2.
# xor    3.
# or     4.
# parent 5.
# append 6.
# each   7.
# set    8.
# let    9.

# has and or xor
distinguish1 ^atom = {
    (true atom .has) ? null : return 3 # xor
    (true atom null) ? return 4 : null # or
    (true atom true) ? 2 : 1           # and / has
}

# parent append each
distinguish2 ^atom = {
    (3.has atom) ? return 5 : null     # parent
    host = []
    host atom 1
    (host.has 0) ? 6 : 7               # append / each
}

# set let
distinguish3 ^atom = {
    p = [ k = 1 ]
    q = [ parent = p ]
    q atom .k 2
    (p.k == 2) ? 8 : 9                 # set / let
}

idAtom ^atom = {
    (true.has atom) ? return (distinguish1 atom) : null
    ([].parent.has atom) ? return (distinguish2 atom) : null
    ([].has atom) ? return (distinguish3 atom) : null
    0
}

# simulating a == b
genericEq ^a b = {
    # If an empty object already has b as a key, we have to get creative
    ([].has b) ? idAtom a == idAtom b : (host = []; host a = true; host.has b)
}

print "genericEq .hello .hello: " (genericEq .hello .hello) ln
print "genericEq .hello .goodbye: " (genericEq .hello .goodbye) ln
print "genericEq .goodbye .hello: " (genericEq .goodbye .hello) ln
print "genericEq .parent .hello: " (genericEq .parent .hello) ln
print "genericEq .hello .parent: " (genericEq .hello .parent) ln
print "genericEq .parent .parent: " (genericEq .parent .parent) ln
print "genericEq .has .let: " (genericEq .has .let) ln
print "genericEq .set .set: " (genericEq .set .set) ln
print "genericEq '1' '1': " (genericEq "1" "1") ln
print "genericEq 1 '1': " (genericEq 1 "1") ln
print "genericEq 1 1: " (genericEq 1 1) ln
print "genericEq '1' 1: " (genericEq "1" 1) ln
print "genericEq 'a' .a: " (genericEq "a" .a) ln
print "genericEq null null: " (genericEq null null) ln
print "genericEq true true: " (genericEq true true) ln
print "genericEq true null: " (genericEq true null) ln
print "genericEq null true: " (genericEq null true) ln
print "genericEq genericEq true: " (genericEq genericEq true) ln
print "genericEq genericEq genericEq: " (genericEq genericEq genericEq) ln
print "genericEq genericEq idAtom: " (genericEq genericEq idAtom) ln

print ln
println "atom ids"
println "--------"
print "has    " (idAtom .has) ln
print "and    " (idAtom .and)  ln
print "xor    " (idAtom .xor) ln
print "or     " (idAtom .or)  ln
print "parent " (idAtom .parent) ln
print "append " (idAtom .append) ln
print "each   " (idAtom .each) ln
print "set    " (idAtom .set) ln
print "let    " (idAtom .let) ln
